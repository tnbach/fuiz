FROM rust:1.76.0 AS builder
WORKDIR /usr/src/fuiz
COPY . .
RUN cargo build --release

# Runtime Stage
FROM ubuntu:22.04
COPY --from=builder /usr/src/fuiz/target/release/fuiz /usr/local/bin/fuiz
EXPOSE 8080
CMD ["fuiz"]
